import React, { PureComponent } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Heading from './Heading';
import Header from './Header';
import SignIn from './SignIn';
import Footer from './Footer';
import Test from './Test';
import { Team } from './Team';
import Auth from './Auth';
import Platform from './Platform';

class App extends PureComponent {
    render() {
        return (
            <>
                <Router>
                    <Route exact path="/" component={Heading} />
                    <Route exact path="/" component={SignIn} />

                    <Route path="/team" component={Team} />
                    <Route path="/auth" component={Auth} />
                    <Route path="/platform" component={Platform} />
                    <Route path="/" component={Header} />
                    <Route exact path="/" component={Test} />
                    <Footer />
                </Router>
            </>
        );
    }
}

export default App;
