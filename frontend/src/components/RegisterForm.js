import React, { PureComponent } from 'react';
import Input from './common/Input';

class RegisterForm extends PureComponent {
    render() {
        return (
            <div className="register-form">
                <Input type="name" id="name">
                    Name
                </Input>
                <Input type="login" id="login">
                    Login
                </Input>
                <Input type="lastname" id="lastname">
                    Last Name
                </Input>
                <Input type="password" id="password">
                    Password
                </Input>
                <Input type="middlename" id="middlename">
                    Middle Name
                </Input>
                <Input type="password" id="password2">
                    Repeat password
                </Input>
                <Input type="date" id="date" />
            </div>
        );
    }
}

export default RegisterForm;
