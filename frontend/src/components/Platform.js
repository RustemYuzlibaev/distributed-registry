import React, { PureComponent } from 'react';

class Platform extends PureComponent {
    render() {
        return (
            <div className="platform">
                <div className="account">
                    <img
                        className="account__avatar"
                        src={'../img/rustem.jpg'}
                    />
                    <h1 className="account__name">Rustem Yuzlibaev</h1>
                    <h3 className="account__income">Earnings: 102 552 rub</h3>
                    <h3 className="account__confirmed-contracts">
                        Confirmed contracts: 34
                    </h3>
                    <h3 className="account__unconfirmed-contracts">
                        Unconfirmed contracts: 21
                    </h3>
                </div>
                <div className="pool">
                    <ul className="pool__list">
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                        <li className="pool__item">Smart contract #1</li>
                        <li className="pool__item">Smart contract #2</li>
                        <li className="pool__item">Smart contract #3</li>
                    </ul>
                </div>
                <div className="contract">
                    <h1 className="contract__id">Contract: #34542</h1>
                    <h2 className="contract__location">Ufa, Administration</h2>
                </div>
            </div>
        );
    }
}

export default Platform;
