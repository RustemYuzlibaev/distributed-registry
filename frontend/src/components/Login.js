import React, { PureComponent } from 'react';
import Input from './common/Input';

class Login extends PureComponent {
    onClick = () => {
        console.log('clicked');

        fetch('http://localhost:3000/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ key: 'value' }),
            mode: 'no-cors',
        });
    };

    render() {
        return (
            <div className="login">
                <h2 className="login__heading">LOG INTO SYSTEM</h2>
                <form className="login__form">
                    <Input type="text" id="login">
                        Login
                    </Input>
                    <Input type="password" id="password">
                        Password
                    </Input>
                    <Input type="submit" onClick={this.onClick}>
                        Log in
                    </Input>
                </form>
            </div>
        );
    }
}

export default Login;
