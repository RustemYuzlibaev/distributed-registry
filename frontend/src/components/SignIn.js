import React, { PureComponent } from 'react';
import Input from './common/Input';

class SingIn extends PureComponent {
    render() {
        return (
            <div className="sign-in">
                <Input type="text" id="login">
                    Login
                </Input>
            </div>
        );
    }
}

export default SingIn;
