import React, { PureComponent } from 'react';

class Input extends PureComponent {
    render() {
        if (this.props.type === 'submit')
            return (
                <input
                    type="submit"
                    value={this.props.children}
                    className="input-btn"
                />
            );
        return (
            <div className="input">
                <input
                    className="input__field"
                    type={this.props.type}
                    id={this.props.id}
                    placeholder=" "
                />
                <label className="input__label" htmlFor={this.props.id}>
                    {this.props.children}
                </label>
                <span className="input__stroke"></span>
            </div>
        );
    }
}

export default Input;
