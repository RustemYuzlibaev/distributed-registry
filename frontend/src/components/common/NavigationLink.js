import React from 'react';
import { Link } from 'react-router-dom';

const NavigationLink = props => {
    return (
        <Link to={props.to} className="navigation__link">
            <li className={props.className}>{props.link}</li>
        </Link>
    );
};

export default NavigationLink;
