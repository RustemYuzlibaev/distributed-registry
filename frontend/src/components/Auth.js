import React, { PureComponent } from 'react';
import RegisterOptions from './RegisterOptions';
import Login from './Login';
import RegiterForm from './RegisterForm';

class Auth extends PureComponent {
    render() {
        return (
            <div className="authorization">
                <div className="auth">
                    <RegisterOptions />
                    <Login />
                </div>
            </div>
        );
    }
}

export default Auth;
