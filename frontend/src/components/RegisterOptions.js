import React, { PureComponent } from 'react';
import styled, { css } from 'styled-components';

const HeaderWrapper = styled.div`
    &::after {
        ${props =>
            props.animation &&
            css`
                animation: reveal-block 1.5s cubic-bezier(0.19, 1, 0.22, 1) 0.5s
                    forwards;
            `}
    }
`;

const Circle = styled.li`
    &:hover .watcher__header--hidden {
        ${props =>
            props.animation &&
            css`
                opacity: 0;
                animation: appear-text 0.0001s linear 1s forwards;
            `}
`;

class RegisterOptions extends PureComponent {
    state = {
        lastTimeStamp: 0,
        animation: true,
        forTheFirstTime: true,
    };

    onHover = e => {
        const enterTimeStamp = Math.round(e.timeStamp / 1000);

        const { forTheFirstTime, lastTimeStamp } = this.state;

        if (lastTimeStamp < enterTimeStamp - 5 || forTheFirstTime) {
            this.setState({ animation: true });
            this.state.forTheFirstTime = false;
        } else {
            this.setState({ animation: false });
        }
    };

    onLeave = e => {
        const lastTimeStamp = Math.round(e.timeStamp / 1000);
        this.state.lastTimeStamp = lastTimeStamp;
    };

    onClick = e => {
        if (/watcher/.test(e.currentTarget.className)) {
        }
        if (/builder/.test(e.currentTarget.className)) {
        }
        if (/servicer/.test(e.currentTarget.className)) {
        }
        if (/provider/.test(e.currentTarget.className)) {
        }
    };

    render() {
        return (
            <div className="register">
                <h1 className="register__heading">YOU ARE...</h1>
                <ul className="register__list">
                    <Circle
                        animation={this.state.animation}
                        className="register__option register__watcher"
                        onMouseEnter={this.onHover}
                        onMouseLeave={this.onLeave}
                        onClick={this.onClick}
                    >
                        <a className="register__link">
                            <i className="fas fa-user-check"></i>
                        </a>
                        <div className="vocation watcher">
                            <HeaderWrapper
                                animation={this.state.animation}
                                className="watcher__header position"
                            >
                                <span className="watcher__header--hidden">
                                    Watcher
                                </span>
                            </HeaderWrapper>
                            <div className="watcher__text">
                                <p className="watcher__text--hidden">
                                    You will participate in voting on deciding
                                    which contractor should win a tender
                                </p>
                            </div>
                        </div>
                    </Circle>

                    <Circle
                        animation={this.state.animation}
                        className="register__option register__builder"
                        onMouseEnter={this.onHover}
                        onMouseLeave={this.onLeave}
                        onClick={this.onClick}
                    >
                        <a className="register__link">
                            <i className="fas fa-hard-hat"></i>
                        </a>
                        <div className="vocation builder">
                            <HeaderWrapper
                                animation={this.state.animation}
                                className="builder__header position"
                            >
                                <span className="watcher__header--hidden">
                                    Builder
                                </span>
                            </HeaderWrapper>
                            <div className="builder__text">
                                <p className="builder__text--hidden">
                                    You are building company that specilizes in
                                    developing infrastructure and corresponding
                                    buildings
                                </p>
                            </div>
                        </div>
                    </Circle>

                    <Circle
                        animation={this.state.animation}
                        className="register__option register__servicer"
                        onMouseEnter={this.onHover}
                        onMouseLeave={this.onLeave}
                        onClick={this.onClick}
                    >
                        <a className="register__link">
                            <i className="fas fa-tools"></i>
                        </a>
                        <div className="vocation servicer">
                            <HeaderWrapper
                                animation={this.state.animation}
                                className="servicer__header position"
                            >
                                <span className="watcher__header--hidden">
                                    Servicer
                                </span>
                            </HeaderWrapper>
                            <div className="servicer__text">
                                <p className="servicer__text--hidden">
                                    Service company that takes full
                                    responsibilty for cleaning areas, improving
                                    infrustructeres as well as conduct serveys
                                </p>
                            </div>
                        </div>
                    </Circle>
                    <Circle
                        animation={this.state.animation}
                        className="register__option register__provider"
                        onMouseEnter={this.onHover}
                        onMouseLeave={this.onLeave}
                        onClick={this.onClick}
                    >
                        <a className="register__link">
                            <i className="fas fa-people-carry"></i>
                        </a>
                        <div className="vocation provider">
                            <HeaderWrapper
                                animation={this.state.animation}
                                className="provider__header position"
                            >
                                <span className="watcher__header--hidden">
                                    Provider
                                </span>
                            </HeaderWrapper>
                            <div className="provider__text">
                                <p className="provider__text--hidden">
                                    As a supplier you can manage all things
                                    regarding with delivering, transporting and
                                    selling
                                </p>
                            </div>
                        </div>
                    </Circle>
                </ul>
            </div>
        );
    }
}

export default RegisterOptions;
