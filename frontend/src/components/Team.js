import React, { PureComponent } from 'react';

export class Team extends PureComponent {
    render() {
        return (
            <div>
                <h1>Meet the team</h1>
                <div className="team">
                    <div className="teammate teammate__one">
                        <h5 className="teammate__name">Rustem Yuzlibaev</h5>
                        <p className="teammate__position">Software Developer</p>
                    </div>
                    <div className="teammate teammate__two">
                        <h5 className="teammate__name">Bulat Gallyamov</h5>
                        <p className="teammate__position">Lawyer</p>
                    </div>
                    <div className="teammate teammate__three">
                        <h5 className="teammate__name">Vadim Kamaletdinov</h5>
                        <p className="teammate__position">Medical Specialist</p>
                    </div>
                    <div className="teammate teammate__four">
                        <h5 className="teammate__name">Rail Yaushev</h5>
                        <p className="teammate__position">Building Engineer</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Team;
