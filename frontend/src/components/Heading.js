import React, { PureComponent } from 'react';

class Heading extends PureComponent {
    render() {
        return (
            <div className="heading">
                <h1 className="headline headline--main">
                    <span className="text">Distributed System</span>
                </h1>
                <h2 className="headline headline--sub">
                    <span className="text">A new way of thinking</span>
                </h2>
            </div>
        );
    }
}

export default Heading;
