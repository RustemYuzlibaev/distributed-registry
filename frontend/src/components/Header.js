import React, { Component } from 'react';
import classNames from 'classnames';
import NavigationLink from './common/NavigationLink';

class Header extends Component {
    state = {
        isOnPage: 'home',
    };

    render() {
        const link = classNames({
            navigation__item: true,
            'navigation__link--active': true,
        });

        return (
            <header className="header">
                <div className="navigation">
                    <div className="navigation__logo">Logo</div>
                    <nav className="navigation__nav">
                        <ul className="navigation__list">
                            <NavigationLink
                                to="/"
                                link="Home"
                                className={link}
                            />
                            <NavigationLink
                                to="/platform"
                                link="Platform"
                                className={link}
                            />
                            <NavigationLink
                                to="/auth"
                                link="Authentication"
                                className={link}
                            />
                            <NavigationLink
                                to="/team"
                                link="Contributors"
                                className={link}
                            />

                            <NavigationLink
                                to="/contact"
                                link="Contact"
                                className={link}
                            />
                        </ul>
                    </nav>
                </div>
            </header>
        );
    }
}

export default Header;
