import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './sass/main.scss';
import './sass/icons/fontawesome.scss';

ReactDOM.render(<App />, document.getElementById('app'));
