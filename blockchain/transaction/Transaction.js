class Transaction {
    constructor() {
        this.id = id;
        this.hash = hash;
        this.data = {
            inputs: [],
            outputs: [],
        };
    }

    toHash() {
        return crypto
            .createHash('sha256')
            .update((this.id + this.data).toString())
            .digest('hex');
    }
}
