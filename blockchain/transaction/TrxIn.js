const crypto = require('crypto');
const verify = crypto.createVerify('SHA256');

class TrxIn {
    constructor(trxOutId, toAddress, amount) {
        this.trxOutId = trxOutId;
        this.toAddress = toAddress;
        this.amount = amount;
    }

    verifySignature() {
        return verify.verify(trxOut.fromAddress, trxOut.signature);
    }
}
