class TransactionList {
    constructor() {
        this.transactionList = [];
    }

    static addTransaction(trx) {
        this.transactionList.push(trx);
    }

    static getTransactionList = () => this.transactionList;
}
