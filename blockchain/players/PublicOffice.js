const Contract = require('../contract/Contract');

class PublicOffice {
    constructor({ publicKey, privateKey }) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    createBuildContract(details) {
        const buildContract = Contract.getContract('building', details);
    }

    createPurchaseContract(details) {
        const purchaseContract = Contract.getContract('purchasing', details);
    }

    createPurchaseContract(details) {
        const serviceContract = Contract.getContract('servicing', details);
    }
}
