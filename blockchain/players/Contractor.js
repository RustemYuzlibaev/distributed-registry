class Contractor {
    constructor({ publicKey, privateKey, type }) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.type = type;
    }
}

export default Contractor;
