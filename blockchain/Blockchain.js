const Block = require('./Block');

class Blockchain {
    constructor() {
        this.blockchain = [];
        this.init();
    }

    init() {
        if (this.blockchain.length === 0) {
            this.blockchain.push(Block.genesis);
        }
    }

    getLastBlock() {
        return this.blockchain[this.blockchain.length - 1];
    }

    addBlock(newBlock) {
        if (Block.checkBlock(newBlock, this.getLastBlock())) {
            this.blockchain.push(newBlock);
        }
    }

    getBlockchain() {
        return this.blockchain;
    }
}
const blockchain = new Blockchain();
let i = 1;

setInterval(() => {
    const block = new Block({
        index: i,
        previousHash: blockchain.getLastBlock().hash,
        transactions: [],
        timestamp: Date.now(),
    });

    i++;
    blockchain.addBlock(block);
    // console.log(blockchain);
    // console.log('\n\n\n\n\n');
}, 2000);
