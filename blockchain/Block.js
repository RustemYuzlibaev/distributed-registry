const crypto = require('crypto');

class Block {
    constructor({ index, previousHash, timestamp, transactions }) {
        this.index = index;
        this.previousHash = previousHash;
        this.transactions = transactions;
        this.timestamp = timestamp;
        this.hash = this.toHash({
            index,
            previousHash,
            timestamp,
            transactions,
        });
    }

    toHash() {
        return crypto
            .createHash('sha256')
            .update(
                (
                    this.index +
                    this.previousHash +
                    this.timestamp +
                    this.transactions
                ).toString(),
            )
            .digest('hex');
    }

    static get genesis() {
        return new Block({
            index: 0,
            previousHash: 0,
            transactions: [],
            timestamp: Date.now(),
        });
    }

    static checkBlock(newBlock, prevBlock) {
        const blockHash = newBlock.toHash();

        if (prevBlock.index + 1 !== newBlock.index) {
            console.error('Invalid index');
            return false;
        }

        if (prevBlock.hash !== newBlock.previousHash) {
            console.error('Invalid previous hash');
            return false;
        }

        if (blockHash !== newBlock.hash) {
            console.error('Invalid block hash');
            return false;
        }

        return true;
    }
}

module.exports = Block;
