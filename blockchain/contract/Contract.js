const BuildContract = require('./types/BuildContract');
const PurchaseContract = require('./types/PurchaseContract');
const ServiceContract = require('./types/ServiceContract');

class Contract {
    static getContract(type, details) {
        if (type === 'building') {
            return new BuildContract(details);
        }

        if (type === 'purchasing') {
            return new PurchaseContract(details);
        }

        if (type === 'servicing') {
            return new ServiceContract(details);
        }

        return null;
    }
}

module.exports = Contract;
