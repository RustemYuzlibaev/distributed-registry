class PurchaseContract {
    constructor({ publicOfficeId, whom, price, description, deadline, dct }) {
        this.publicOfficeId = publicOfficeId;
        this.whom = whom;
        this.price = price;
        this.description = description;
        this.deadline = deadline;
        this.dct = dct;
        this.providers = [];
    }

    addProvider(contractor) {
        this.providers.push(contractor);
    }
}
