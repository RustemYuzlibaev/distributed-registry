class BuildContract {
    constructor({
        publicOfficeId,
        price,
        location,
        description,
        deadline,
        dct,
    }) {
        this.publicOfficeID = publicOfficeID;
        this.price = price;
        this.location = location;
        this.description = description;
        this.deadline = deadline;
        this.dct = dct;
        this.builders = [];
    }

    addBuilder(contractor) {
        this.builders.push(contractor);
    }
}
