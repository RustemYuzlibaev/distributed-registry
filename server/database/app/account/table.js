const pool = require('../../databasePool');

class AccountTable {
    static storeAccount({
        login,
        password,
        phone,
        name,
        surname,
        middlename,
        avatar,
        age,
        location,
    }) {
        return new Promise((resolve, reject) => {
            pool.query(
                `INSERT INTO account(login, password, phone, name, surname, middlename, avatar, age, location) VALUES($1, $2, $3, $4, $5, $6, $7, $8)`,
                [
                    login,
                    password,
                    phone,
                    name,
                    surname,
                    middlename,
                    avatar,
                    age,
                    location,
                ],
                (err, res) => {
                    if (err) return reject(err);
                    resolve();
                },
            );
        });
    }
}
