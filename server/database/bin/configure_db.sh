#!/bin/bash

echo "Configuring Application Database..."

export PGPASSWORD="node_password"

dropdb -U node_user application
create -U node_user application 

psql -U node_user application < ./bin/sql/watcher.sql
psql -U node_user application < ./bin/sql/builder.sql
psql -U node_user application < ./bin/sql/servicer.sql
psql -U node_user application < ./bin/sql/provider.sql

echo "Application DB configured."