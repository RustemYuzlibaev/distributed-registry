CREATE TABLE watcher
(
    id SERIAL PRIMARY KEY,
    "login" CHARACTER(16),
    "password" CHARACTER(64),
    "phone" INTEGER,
    "name" CHARACTER(16),
    "lastname" CHARACTER(16),
    "middlename" CHARACTER(16),
    "age" CHARACTER(16),
    "location" CHARACTER(64)
);