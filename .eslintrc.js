module.exports = {
    root: true,
    extends: ['airbnb', 'prettier'],
    rules: {
        'import/no-extraneous-dependencies': [
            'error',
            { devDependencies: true },
        ],
        'no-restricted-syntax': ['error', 'LabeledStatement', 'WithStatement'],
        'prefer-spread': 'off',
        'no-underscore-dangle': 'off',
        'func-names': 'off',
        'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
        'react/jsx-indent': true,
    },
    env: {
        node: true,
        browser: true,
    },
};
